﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Mdm.Context.Entities;

namespace Mdm.Context
{
    public partial class MdmContext : DbContext
    {
        public MdmContext()
        {
        }

        public MdmContext(DbContextOptions<MdmContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ApolloAccountType> ApolloAccountType { get; set; }
        public virtual DbSet<ApolloAcctAssignmentGroupType> ApolloAcctAssignmentGroupType { get; set; }
        public virtual DbSet<ApolloBlockBillingType> ApolloBlockBillingType { get; set; }
        public virtual DbSet<ApolloBlockDeliveryType> ApolloBlockDeliveryType { get; set; }
        public virtual DbSet<ApolloBlockOrderType> ApolloBlockOrderType { get; set; }
        public virtual DbSet<ApolloCompanyCodeType> ApolloCompanyCodeType { get; set; }
        public virtual DbSet<ApolloCreditRepGroupType> ApolloCreditRepGroupType { get; set; }
        public virtual DbSet<ApolloCustomerClassType> ApolloCustomerClassType { get; set; }
        public virtual DbSet<ApolloCustomerGroupType> ApolloCustomerGroupType { get; set; }
        public virtual DbSet<ApolloCustomerPriceProcType> ApolloCustomerPriceProcType { get; set; }
        public virtual DbSet<ApolloDeliveryPriorityType> ApolloDeliveryPriorityType { get; set; }
        public virtual DbSet<ApolloDistLevelType> ApolloDistLevelType { get; set; }
        public virtual DbSet<ApolloDistributionChannelType> ApolloDistributionChannelType { get; set; }
        public virtual DbSet<ApolloDivisionType> ApolloDivisionType { get; set; }
        public virtual DbSet<ApolloIncoTermsType> ApolloIncoTermsType { get; set; }
        public virtual DbSet<ApolloIndustryCodeType> ApolloIndustryCodeType { get; set; }
        public virtual DbSet<ApolloIndustryType> ApolloIndustryType { get; set; }
        public virtual DbSet<ApolloPartnerFunctionType> ApolloPartnerFunctionType { get; set; }
        public virtual DbSet<ApolloPaymentTermsType> ApolloPaymentTermsType { get; set; }
        public virtual DbSet<ApolloPpcustProcType> ApolloPpcustProcType { get; set; }
        public virtual DbSet<ApolloPriceListType> ApolloPriceListType { get; set; }
        public virtual DbSet<ApolloReconAccountType> ApolloReconAccountType { get; set; }
        public virtual DbSet<ApolloRiskCategoryType> ApolloRiskCategoryType { get; set; }
        public virtual DbSet<ApolloSalesOfficeType> ApolloSalesOfficeType { get; set; }
        public virtual DbSet<ApolloShippingConditionsType> ApolloShippingConditionsType { get; set; }
        public virtual DbSet<ApolloShippingCustomerType> ApolloShippingCustomerType { get; set; }
        public virtual DbSet<ApolloSpecialPricingType> ApolloSpecialPricingType { get; set; }
        public virtual DbSet<CategoryType> CategoryType { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<MdmTeam> MdmTeam { get; set; }
        public virtual DbSet<MdmTeamUser> MdmTeamUser { get; set; }
        public virtual DbSet<MdmUser> MdmUser { get; set; }
        public virtual DbSet<RoleType> RoleType { get; set; }
        public virtual DbSet<SalesOrgType> SalesOrgType { get; set; }
        public virtual DbSet<SicNaicsMapping> SicNaicsMapping { get; set; }
        public virtual DbSet<SystemFieldMapping> SystemFieldMapping { get; set; }
        public virtual DbSet<SystemFieldMdmTeam> SystemFieldMdmTeam { get; set; }
        public virtual DbSet<SystemFieldType> SystemFieldType { get; set; }
        public virtual DbSet<SystemRoleMapping> SystemRoleMapping { get; set; }
        public virtual DbSet<SystemSalesOrgMapping> SystemSalesOrgMapping { get; set; }
        public virtual DbSet<SystemType> SystemType { get; set; }
        public virtual DbSet<Workflow> Workflow { get; set; }
        public virtual DbSet<WorkflowApolloBlock> WorkflowApolloBlock { get; set; }
        public virtual DbSet<WorkflowApolloBlockDocument> WorkflowApolloBlockDocument { get; set; }
        public virtual DbSet<WorkflowApolloContracts> WorkflowApolloContracts { get; set; }
        public virtual DbSet<WorkflowApolloCredit> WorkflowApolloCredit { get; set; }
        public virtual DbSet<WorkflowApolloCreditDocument> WorkflowApolloCreditDocument { get; set; }
        public virtual DbSet<WorkflowApolloCustomerMaster> WorkflowApolloCustomerMaster { get; set; }
        public virtual DbSet<WorkflowApolloGlobalTrade> WorkflowApolloGlobalTrade { get; set; }
        public virtual DbSet<WorkflowApolloPartner> WorkflowApolloPartner { get; set; }
        public virtual DbSet<WorkflowApolloPricing> WorkflowApolloPricing { get; set; }
        public virtual DbSet<WorkflowApolloUpdatesDeprecated> WorkflowApolloUpdatesDeprecated { get; set; }
        public virtual DbSet<WorkflowCustomerGlobal> WorkflowCustomerGlobal { get; set; }
        public virtual DbSet<WorkflowCustomerGlobalDocument> WorkflowCustomerGlobalDocument { get; set; }
        public virtual DbSet<WorkflowDocument> WorkflowDocument { get; set; }
        public virtual DbSet<WorkflowExtendToNewSalesOrg> WorkflowExtendToNewSalesOrg { get; set; }
        public virtual DbSet<WorkflowExtendToNewSalesOrgDeprecated> WorkflowExtendToNewSalesOrgDeprecated { get; set; }
        public virtual DbSet<WorkflowExtendToNewSystem> WorkflowExtendToNewSystem { get; set; }
        public virtual DbSet<WorkflowPartner> WorkflowPartner { get; set; }
        public virtual DbSet<WorkflowStateType> WorkflowStateType { get; set; }
        public virtual DbSet<WorkflowTask> WorkflowTask { get; set; }
        public virtual DbSet<WorkflowTaskStateType> WorkflowTaskStateType { get; set; }
        public virtual DbSet<WorkflowTaskType> WorkflowTaskType { get; set; }
        public virtual DbSet<WorkflowTaskTypeTeam> WorkflowTaskTypeTeam { get; set; }
        public virtual DbSet<WorkflowType> WorkflowType { get; set; }
        public virtual DbSet<WorkflowTypeUserNotification> WorkflowTypeUserNotification { get; set; }
        public virtual DbSet<WorkflowUpdateContracts> WorkflowUpdateContracts { get; set; }
        public virtual DbSet<WorkflowUpdates> WorkflowUpdates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApolloAccountType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloAcctAssignmentGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockBillingType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockDeliveryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloBlockOrderType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCompanyCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCreditRepGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerClassType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerGroupType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloCustomerPriceProcType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDeliveryPriorityType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDistLevelType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloDistributionChannelType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ApolloDivisionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<ApolloIncoTermsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloIndustryCodeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloIndustryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPartnerFunctionType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPaymentTermsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPpcustProcType>(entity =>
            {
                entity.ToTable("ApolloPPCustProcType");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloPriceListType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloReconAccountType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloRiskCategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloSalesOfficeType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloShippingConditionsType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloShippingCustomerType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ApolloSpecialPricingType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<CategoryType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<MdmTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<MdmTeamUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.MdmTeamId).HasColumnType("int(11)");

                entity.Property(e => e.MdmUserId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MdmUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Email2).HasColumnType("varchar(200)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<RoleType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<SalesOrgType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<SicNaicsMapping>(entity =>
            {
                entity.HasKey(e => e.SicCode)
                    .HasName("PRIMARY");

                entity.Property(e => e.SicCode).HasColumnType("varchar(12)");

                entity.Property(e => e.NaicsCode)
                    .IsRequired()
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.NaicsCodeDescription)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.SicCodeDescription)
                    .IsRequired()
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<SystemFieldMapping>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DecimalPlaces).HasColumnType("int(11)");

                entity.Property(e => e.FriendlyFieldName)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.IsRequired).HasColumnType("bit(1)");

                entity.Property(e => e.MaxLength).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.SystemFieldTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemId).HasColumnType("int(11)");

                entity.Property(e => e.TechnicalFieldName)
                    .IsRequired()
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<SystemFieldMdmTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.MdmTeamId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<SystemFieldType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<SystemRoleMapping>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<SystemSalesOrgMapping>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<SystemType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowRequestorId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowStateTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowApolloBlock>(entity =>
            {
                entity.HasIndex(e => e.Id)
                    .HasName("Id_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BlockAllSalesOrg).HasColumnType("bit(1)");

                entity.Property(e => e.BlockBillingTypeId).HasColumnType("int(11)");

                entity.Property(e => e.BlockDeliveryTypeId).HasColumnType("int(11)");

                entity.Property(e => e.BlockOrderTypeId).HasColumnType("int(11)");

                entity.Property(e => e.BlockPosting).HasColumnType("bit(1)");

                entity.Property(e => e.BlockReason)
                    .IsRequired()
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemAccountNumber)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloBlockDocument>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DocumentLocation)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloContracts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountTypeId).HasColumnType("int(11)");

                entity.Property(e => e.AdditionalNotes).HasColumnType("varchar(500)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerGroupTypeId).HasColumnType("int(11)");

                entity.Property(e => e.IncoTermsTypeId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PaymentTermsTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloCredit>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AdditionalNotes).HasColumnType("varchar(500)");

                entity.Property(e => e.ContactEmail).HasColumnType("varchar(100)");

                entity.Property(e => e.ContactFax).HasColumnType("char(10)");

                entity.Property(e => e.ContactFirstName).HasColumnType("char(12)");

                entity.Property(e => e.ContactLastName).HasColumnType("char(12)");

                entity.Property(e => e.ContactTelephone).HasColumnType("char(10)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CredInfoNumber).HasColumnType("varchar(100)");

                entity.Property(e => e.CreditLimit).HasColumnType("decimal(15,2)");

                entity.Property(e => e.CreditRepGroupTypeId).HasColumnType("int(11)");

                entity.Property(e => e.LastExtReview).HasColumnType("varchar(100)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PaymentIndex).HasColumnType("varchar(100)");

                entity.Property(e => e.PaymentTermsTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Rating).HasColumnType("varchar(100)");

                entity.Property(e => e.RiskCategoryTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloCreditDocument>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DocumentLocation)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloCustomerMaster>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountStatement)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.AccountTypeId).HasColumnType("int(11)");

                entity.Property(e => e.AcctAssignmentGroupTypeId).HasColumnType("int(11)");

                entity.Property(e => e.AcctgClerk)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.AdditionalNotes).HasColumnType("varchar(500)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerClassTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerGroupTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerPriceProcTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DeliveryPriorityTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Incoterms1TypeId).HasColumnType("int(11)");

                entity.Property(e => e.Incoterms2).HasColumnType("char(28)");

                entity.Property(e => e.IndustryCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.IndustryTypeId).HasColumnType("int(11)");

                entity.Property(e => e.License).HasColumnType("varchar(30)");

                entity.Property(e => e.LicenseExpDate).HasColumnType("date");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.OrderCombination).HasColumnType("bit(1)");

                entity.Property(e => e.PartnerFunctionNumber).HasColumnType("varchar(20)");

                entity.Property(e => e.PartnerFunctionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.PaymentHistoryRecord)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PaymentMethods)
                    .IsRequired()
                    .HasColumnType("char(10)");

                entity.Property(e => e.PpcustProcTypeId)
                    .HasColumnName("PPCustProcTypeId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceListTypeId).HasColumnType("int(11)");

                entity.Property(e => e.ReconAccountTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOfficeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SearchTerm1).HasColumnType("varchar(100)");

                entity.Property(e => e.SearchTerm2).HasColumnType("varchar(100)");

                entity.Property(e => e.ShippingConditionsTypeId).HasColumnType("int(11)");

                entity.Property(e => e.ShippingCustomerTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SortKey)
                    .IsRequired()
                    .HasColumnType("char(3)");

                entity.Property(e => e.TaxClassification)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.TaxNumber2).HasColumnType("varchar(20)");

                entity.Property(e => e.TransporationZone)
                    .IsRequired()
                    .HasColumnType("char(4)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloGlobalTrade>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AdditionalNotes).HasColumnType("varchar(500)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloPartner>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SoldToWorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloPricing>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AdditionalNotes).HasColumnType("varchar(500)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DistLevelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.SpecialPricingTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowApolloUpdatesDeprecated>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.Deltas)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowCustomerGlobal>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CategoryTypeId).HasColumnType("int(11)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CompanyCodeTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DistributionChannelTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DivisionTypeId).HasColumnType("int(11)");

                entity.Property(e => e.DunsNumber).HasColumnType("varchar(50)");

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasColumnType("varchar(200)");

                entity.Property(e => e.Fax).HasColumnType("varchar(50)");

                entity.Property(e => e.MdmCustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.NaicsCode).HasColumnType("varchar(50)");

                entity.Property(e => e.NaicsDescription).HasColumnType("varchar(200)");

                entity.Property(e => e.Name1)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Name2).HasColumnType("varchar(200)");

                entity.Property(e => e.Name3).HasColumnType("varchar(200)");

                entity.Property(e => e.Name4).HasColumnType("varchar(200)");

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasColumnType("char(10)");

                entity.Property(e => e.Purpose).HasColumnType("varchar(1000)");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SicCode4).HasColumnType("varchar(100)");

                entity.Property(e => e.SicCode6).HasColumnType("varchar(100)");

                entity.Property(e => e.SicCode8).HasColumnType("varchar(100)");

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasColumnType("varchar(35)");

                entity.Property(e => e.Street2).HasColumnType("varchar(35)");

                entity.Property(e => e.SystemRecordId).HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.TaxJurisdiction)
                    .IsRequired()
                    .HasColumnType("char(15)");

                entity.Property(e => e.TaxNumber).HasColumnType("varchar(20)");

                entity.Property(e => e.Telephone).HasColumnType("varchar(50)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.VatRegNo).HasColumnType("varchar(20)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowCustomerGlobalDocument>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DocumentLocation)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowDocument>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DocumentTypeId).HasColumnType("int(11)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.S3objectKey)
                    .HasColumnName("S3ObjectKey")
                    .HasColumnType("longtext");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowExtendToNewSalesOrg>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerName).HasColumnType("varchar(200)");

                entity.Property(e => e.Deltas)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.MdmCustomerId).HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SourceSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.TargetSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowExtendToNewSalesOrgDeprecated>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.MdmCustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SourceSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId).HasColumnType("int(11)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.TargetSalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowExtendToNewSystem>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.MdmCustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.SourceSystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.TargetSystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowPartner>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.PartnerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PartnerRoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SoldToId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowStateType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowTask>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.TeamId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WorkflowTaskNote).HasColumnType("varchar(1000)");

                entity.Property(e => e.WorkflowTaskStateTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTaskTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowTaskStateType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowTaskType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.TaskCompletionRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowOrder).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowTaskTypeTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AllowBusinessRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.MdmTeamId).HasColumnType("int(11)");

                entity.Property(e => e.TaskTeamRuleName).HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowTaskTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CanAutoComplete).HasColumnType("bit(1)");

                entity.Property(e => e.Description).HasColumnType("varchar(100)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.WorkflowStartRuleName).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<WorkflowTypeUserNotification>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowTypeId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<WorkflowUpdateContracts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<WorkflowUpdates>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.CreatedUserId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerName).HasColumnType("varchar(200)");

                entity.Property(e => e.Deltas)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.MdmCustomerId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ModifiedUserId).HasColumnType("int(11)");

                entity.Property(e => e.RoleTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SalesOrgTypeId).HasColumnType("int(11)");

                entity.Property(e => e.SystemRecordId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SystemTypeId).HasColumnType("int(11)");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });
        }
    }
}
