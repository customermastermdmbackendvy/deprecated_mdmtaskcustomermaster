﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowExtendToNewSystem
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string MdmCustomerId { get; set; }
        public int WorkflowTypeId { get; set; }
        public int SourceSystemTypeId { get; set; }
        public int TargetSystemTypeId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
