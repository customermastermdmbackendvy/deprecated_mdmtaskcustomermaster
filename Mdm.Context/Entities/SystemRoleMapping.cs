﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class SystemRoleMapping
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public int RoleTypeId { get; set; }
    }
}
