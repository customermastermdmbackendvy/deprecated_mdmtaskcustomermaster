﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowType
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string WorkflowStartRuleName { get; set; }
        public bool CanAutoComplete { get; set; }
    }
}
