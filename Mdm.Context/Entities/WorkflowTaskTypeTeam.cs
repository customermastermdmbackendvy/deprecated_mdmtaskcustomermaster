﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowTaskTypeTeam
    {
        public int Id { get; set; }
        public int WorkflowTaskTypeId { get; set; }
        public int MdmTeamId { get; set; }
        public string AllowBusinessRuleName { get; set; }
        public string TaskTeamRuleName { get; set; }
    }
}
