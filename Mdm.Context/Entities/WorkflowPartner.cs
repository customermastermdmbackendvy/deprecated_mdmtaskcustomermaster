﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowPartner
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public int? SalesOrgTypeId { get; set; }
        public string SoldToId { get; set; }
        public string PartnerId { get; set; }
        public int PartnerRoleTypeId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
