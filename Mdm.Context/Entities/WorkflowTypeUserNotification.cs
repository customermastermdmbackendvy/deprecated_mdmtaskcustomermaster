﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowTypeUserNotification
    {
        public int Id { get; set; }
        public int WorkflowTypeId { get; set; }
        public int UserId { get; set; }
    }
}
