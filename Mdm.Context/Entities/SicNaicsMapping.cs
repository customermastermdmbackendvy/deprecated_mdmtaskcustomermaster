﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class SicNaicsMapping
    {
        public string SicCode { get; set; }
        public string SicCodeDescription { get; set; }
        public string NaicsCode { get; set; }
        public string NaicsCodeDescription { get; set; }
    }
}
