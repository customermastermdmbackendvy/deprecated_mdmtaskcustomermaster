﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowExtendToNewSalesOrgDeprecated
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string MdmCustomerId { get; set; }
        public int WorkflowTypeId { get; set; }
        public int SystemTypeId { get; set; }
        public int SystemRecordId { get; set; }
        public int RoleTypeId { get; set; }
        public int SourceSalesOrgTypeId { get; set; }
        public int TargetSalesOrgTypeId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
