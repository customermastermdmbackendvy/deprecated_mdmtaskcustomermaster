﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloPartner
    {
        public int Id { get; set; }
        public string SoldToWorkflowId { get; set; }
        public string PartnerId { get; set; }
        public int RoleTypeId { get; set; }
        public int? SalesOrgTypeId { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
