﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class Workflow
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public int WorkflowTypeId { get; set; }
        public int WorkflowRequestorId { get; set; }
        public int WorkflowStateTypeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
