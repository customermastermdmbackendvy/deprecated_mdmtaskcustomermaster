﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowTaskType
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int WorkflowTypeId { get; set; }
        public int? WorkflowOrder { get; set; }
        public string TaskCompletionRuleName { get; set; }
    }
}
