﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloTax
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string TaxJurisdiction { get; set; }
        public string AdditionalNotes { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
