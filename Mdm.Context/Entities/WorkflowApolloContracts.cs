﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowApolloContracts
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public int? PaymentTermsTypeId { get; set; }
        public int? IncoTermsTypeId { get; set; }
        public int CustomerGroupTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public string AdditionalNotes { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
