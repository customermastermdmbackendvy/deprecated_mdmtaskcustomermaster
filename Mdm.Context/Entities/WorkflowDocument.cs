﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowDocument
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentName { get; set; }
        public string S3objectKey { get; set; }
        public bool IsActive { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
