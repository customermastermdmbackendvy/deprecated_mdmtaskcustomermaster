﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class SystemSalesOrgMapping
    {
        public int Id { get; set; }
        public int SystemTypeId { get; set; }
        public int SalesOrgTypeId { get; set; }
    }
}
