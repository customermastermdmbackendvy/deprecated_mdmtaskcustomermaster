﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class WorkflowCustomerGlobalDocument
    {
        public int Id { get; set; }
        public string WorkflowId { get; set; }
        public string DocumentLocation { get; set; }
        public string DocumentName { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
