﻿using System;
using System.Collections.Generic;

namespace Mdm.Context.Entities
{
    public partial class SystemFieldMdmTeam
    {
        public int Id { get; set; }
        public string FieldName { get; set; }
        public int MdmTeamId { get; set; }
    }
}
