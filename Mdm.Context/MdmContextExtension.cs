﻿
using Microsoft.EntityFrameworkCore;

namespace Mdm.Context
{
    public partial class MdmContext : DbContext
    {
        private readonly string _connectionString;
        public MdmContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
           => optionsBuilder.UseMySQL(_connectionString);
    }
}
