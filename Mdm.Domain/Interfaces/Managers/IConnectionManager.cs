﻿using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface IConnectionManager
    {
        string ConnectionString { get;}
        Task<string> GetConnectionString();
    }
}
