﻿
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface ISecretsManager
    {
        Task<string> GetSecret();
    }
}
