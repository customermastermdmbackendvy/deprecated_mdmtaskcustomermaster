﻿
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface ISnsManager
    {
        Task<bool> SendSnsMessage<T>(T message) where T : class;
    }
}
