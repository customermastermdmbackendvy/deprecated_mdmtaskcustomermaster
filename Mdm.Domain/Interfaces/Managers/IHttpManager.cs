﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Managers
{
    public interface IHttpManager
    {
        Task<bool> UpdateWorkflowData<T>(T workflowRequest) where T : class;

    }
}
