﻿

using Mdm.Domain.Entities;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface IUserRepository : IBaseRepository
    {
        Task<MdmUserDomain> Retrieve(string userId);
    }
}
