﻿using Mdm.Domain.Entities;
using System.Threading.Tasks;

namespace Mdm.Domain.Interfaces.Repositories
{
    public interface ICustomerRepository : IBaseRepository
    {
        WorkflowApolloCustomerMasterDomain Retrieve(string workflowId);
        Task<bool> Update(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain);
        Task<bool> Create(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain);

        WorkflowCustomerGlobalDomain RetrieveCustomerGlobal(string workflowId);
        Task<bool> UpdateCustomerGlobal(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain);
        Task<bool> CreateCustomerGlobal(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain);

    }
}
