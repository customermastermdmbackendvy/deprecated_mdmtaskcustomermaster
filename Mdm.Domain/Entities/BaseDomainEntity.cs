﻿

using System;

namespace Mdm.Domain.Entities
{
    public class BaseDomainEntity
    {
        public string UserId { get; set; }
        public int Id { get; set; }
        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
