﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mdm.Domain.Entities
{
   public partial class WorkflowCustomerGlobalDomain: BaseDomainEntity
    {
        public string WorkflowId { get; set; }
        public string MdmCustomerId { get; set; }
        public string SystemRecordId { get; set; }
        public string Title { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public string Street { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string VatRegNo { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TaxNumber { get; set; }
        public string DunsNumber { get; set; }
        public string NaicsCode { get; set; }
        public string NaicsDescription { get; set; }
        public string SicCode4 { get; set; }
        public string SicCode6 { get; set; }
        public string SicCode8 { get; set; }
        public string Purpose { get; set; }
        public int CategoryTypeId { get; set; }
        public int? RoleTypeId { get; set; }
        public int? SalesOrgTypeId { get; set; }
        public int SystemTypeId { get; set; }
        public int CompanyCodeTypeId { get; set; }
        public int DistributionChannelTypeId { get; set; }
        public int DivisionTypeId { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
