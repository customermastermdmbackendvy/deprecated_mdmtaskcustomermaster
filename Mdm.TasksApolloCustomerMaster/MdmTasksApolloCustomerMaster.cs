
using System;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Mdm.Infrastructure;
using Mdm.Services.Models;
using Microsoft.Extensions.DependencyInjection;
using Mdm.Services.Interfaces;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Mdm.TasksApolloCustomerMaster
{
    public class MdmTasksApolloCustomerMasterDev
    {
        public MdmTasksApolloCustomerMasterDev()
        {
            LambdaLogger.Log("Initiating Customer Update Lambda.");
            ApplicationSetup.Instance.BuildMappers().SetupDependencies();
        }
        public async Task<OperationResult<WorkflowApolloCustomerModel>> SaveTaskCustomerMaster(WorkflowApolloCustomerModel workflowApolloCustomerMasterModel, ILambdaContext context)
        {
            LambdaLogger.Log("Initiating SaveTaskGlobalCustomerMaster operation for Apollo.");
            var workflowEngine = ApplicationSetup.Instance.ServiceProvider.GetService<ICustomerService>();
            try
            {
                return await workflowEngine.ProcessApolloTaskCustomerMaster(workflowApolloCustomerMasterModel);
            }
            catch (Exception ex)
            {
              workflowEngine.ProcessOperationError("Error with SaveTaskGlobalCustomerMaster. " + ex.Message, "SaveTaskGlobalCustomerMaster");
              return workflowEngine.OperationResult;
            }
        }
    }
}
