﻿using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Infrastructure.Managers;
using Mdm.Infrastructure.Repositories;
using Mdm.Services;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Mdm.Infrastructure
{
    public class ApplicationSetup
    {
        private ServiceCollection _services;
        private IMapper _serviceMapper;

        private IMapper _repoMapper;
        public  IServiceProvider ServiceProvider { get; private set; }

        private static readonly Lazy<ApplicationSetup> Instancelock = new Lazy<ApplicationSetup>(() => new ApplicationSetup());

        #region Private Constructor
        private ApplicationSetup()
        {
            LambdaLogger.Log("Instatiating Application Setup.");
        }
        #endregion

        #region Public Methods
        public static ApplicationSetup Instance
        {
            get
            {
                return Instancelock.Value;
            }
        }

        public ApplicationSetup BuildMappers()
        {
            _repoMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WorkflowApolloCustomerMasterDomain, WorkflowApolloCustomerMaster>();

                cfg.CreateMap<WorkflowApolloCustomerMaster, WorkflowApolloCustomerMasterDomain>();

                cfg.CreateMap<WorkflowCustomerGlobalDomain, WorkflowCustomerGlobal>();

                cfg.CreateMap<WorkflowCustomerGlobal, WorkflowCustomerGlobalDomain>();

                cfg.CreateMap<MdmUserDomain, MdmUser>();
                cfg.CreateMap<MdmUser, MdmUserDomain>();

            }).CreateMapper();

            _serviceMapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WorkflowApolloCustomerModel, WorkflowApolloCustomerMasterDomain>()
                        .ForMember(dest => dest.WorkflowId, opt => opt.MapFrom(src => src.WorkflowTaskModel == null ? string.Empty : src.WorkflowTaskModel.WorkflowId))
                        .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.WorkflowTaskModel == null ? string.Empty : src.WorkflowTaskModel.UserId))
                        ;

                cfg.CreateMap<WorkflowApolloCustomerModel, WorkflowCustomerGlobalDomain>()
                        .ForMember(dest => dest.WorkflowId, opt => opt.MapFrom(src => src.WorkflowTaskModel == null ? string.Empty : src.WorkflowTaskModel.WorkflowId))
                        .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.WorkflowTaskModel == null ? string.Empty : src.WorkflowTaskModel.UserId));
            }).CreateMapper();



            return this;
        }

        public ApplicationSetup SetupDependencies()
        {
            _services = new ServiceCollection();
            ConfigureServices();
            ServiceProvider = _services.BuildServiceProvider();
            return this;
        }
        #endregion

        #region Helpers
        private void ConfigureServices()
        {
            _services.AddScoped<ISnsManager, SnsManager>();
            _services.AddScoped<ISecretsManager, SecretsManager>();
            _services.AddScoped<IHttpManager, HttpManager>();
            _services.AddSingleton<IConnectionManager, ConnectionManager>();
            _services.AddScoped<ICustomerRepository, CustomerRepository>(cr => new CustomerRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<IUserRepository, UserRepository>(cr => new UserRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<IBaseRepository, BaseRepository>(cr => new CustomerRepository(cr.GetRequiredService<IConnectionManager>(), _repoMapper));
            _services.AddScoped<ICustomerService, CustomerService>(cs => new CustomerService(cs.GetRequiredService<ICustomerRepository>(), cs.GetRequiredService<ICustomerRepository>(), cs.GetRequiredService<IUserRepository>(), cs.GetRequiredService<ISnsManager>(),  _serviceMapper));
        }
        #endregion
    }
}
