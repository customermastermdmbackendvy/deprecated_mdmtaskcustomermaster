﻿

namespace Mdm.Infrastructure.Managers
{
    public class DataBaseConnection
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Engine { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string DbInstanceIdentifier { get; set; }
        public string DataBase { get; set; }
    }
}


