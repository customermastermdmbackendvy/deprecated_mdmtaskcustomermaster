﻿

using System;
using System.Text;
using System.IO;
using System.Linq;
using Amazon;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Mdm.Infrastructure.Helpers.Constants;
using Mdm.Domain.Interfaces.Managers;

namespace Mdm.Infrastructure.Managers
{
    public class SecretsManager  : ISecretsManager
    {
        private readonly string _rdsSecretName;
        private readonly string _environmentRegion;

        public SecretsManager()
        {
            _rdsSecretName = Environment.GetEnvironmentVariable(EnvironmentVariables.RdsSecretName);
            _environmentRegion = Environment.GetEnvironmentVariable(EnvironmentVariables.EnvironmentRegion);
        }

        public async Task<string> GetSecret()
        {
            using (var client = new AmazonSecretsManagerClient(region: RegionEndpoint.GetBySystemName(_environmentRegion)))
            {
                var request = new GetSecretValueRequest
                {
                    SecretId = _rdsSecretName,
                    VersionStage = "AWSCURRENT" // VersionStage defaults to AWSCURRENT if unspecified.
                };

                GetSecretValueResponse response = null;

                try
                {
                    response = await client.GetSecretValueAsync(request);
                }
                catch (DecryptionFailureException e)
                {
                    // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    LambdaLogger.Log("DecryptionFailureException encountered on secret manager utility " + e.Message);
                }
                catch (InternalServiceErrorException e)
                {
                    // An error occurred on the server side.
                    LambdaLogger.Log("InternalServiceErrorException encountered on secret manager utility " + e.Message);
                }
                catch (InvalidParameterException e)
                {
                    LambdaLogger.Log("InternalServiceErrorException encountered on secret manager utility " + e.Message);
                }
                catch (InvalidRequestException e)
                {
                    // You provided a parameter value that is not valid for the current state of the resource.
                    LambdaLogger.Log("InvalidRequestException encountered on secret manager utility " + e.Message);
                }
                catch (ResourceNotFoundException e)
                {
                    // We can't find the resource that you asked for.
                    LambdaLogger.Log("ResourceNotFoundException encountered on secret manager utility " + e.Message);
                }
                catch (AggregateException e)
                {
                    // More than one of the above exceptions were triggered.
                    // Deal with the exception here, and/or rethrow at your discretion.
                    LambdaLogger.Log("AggregateException encountered on secret manager utility " + e.Message);
                }
                catch (Exception e)
                {
                    LambdaLogger.Log("Unhandled error with secret manager utility " + e.Message);
                }

                // Decrypts secret using the associated KMS CMK.
                // Depending on whether the secret is a string or binary, one of these fields will be populated.
                if (response.SecretString != null)
                {
                    LambdaLogger.Log("Retrieving connection secret.");
                    return response.SecretString;
                }
                else
                {
                    var reader = new StreamReader(response.SecretBinary);
                    return Encoding.UTF8.GetString(Convert.FromBase64String(reader.ReadToEnd()));
                }
            }

        }
    }
}
