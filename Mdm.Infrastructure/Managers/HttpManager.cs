﻿using Amazon.Lambda.Core;
using Flurl;
using Flurl.Http;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Infrastructure.Helpers.Constants;
using Mdm.Services.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Managers
{
    public class HttpManager : IHttpManager
    {
        private readonly string _workflowUpdateUrl;

        public HttpManager()
        {
            _workflowUpdateUrl = Environment.GetEnvironmentVariable(EnvironmentVariables.WorkflowUpdateUrl);
        }

        public async Task<bool> UpdateWorkflowData<T>(T workflowRequest) where T : class
        {
            try
            {
                var result = await new Url(_workflowUpdateUrl)
                    .WithTimeout(30)
                    .PostJsonAsync(workflowRequest).ReceiveJson<OperationResult<Object>>();
                return result.IsSuccess;
            }
            catch (Exception ex)
            {
                LambdaLogger.Log("Error with updating the Workflow Engine.  Details: " + ex.Message);
                return false;
            }
        }
    }
}