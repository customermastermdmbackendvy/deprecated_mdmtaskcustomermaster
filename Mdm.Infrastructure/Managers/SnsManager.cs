﻿
using System;
using Amazon;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Newtonsoft.Json;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Infrastructure.Helpers.Constants;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;

namespace Mdm.Infrastructure.Managers
{
    public class SnsManager : ISnsManager
    {
        private readonly string _environmentRegion;
        private readonly string _snsWorkflowApproveTaskArn;

        public SnsManager()
        {
            _snsWorkflowApproveTaskArn = Environment.GetEnvironmentVariable(EnvironmentVariables.SnsUpdateWorkflowArn);
            _environmentRegion = Environment.GetEnvironmentVariable(EnvironmentVariables.EnvironmentRegion);
        }

        public async Task<bool> SendSnsMessage<T>(T message) where T : class
        {
            using (var client = new AmazonSimpleNotificationServiceClient(region: RegionEndpoint.GetBySystemName(_environmentRegion)))
            {
                var request = new PublishRequest
                {
                    Message   = JsonConvert.SerializeObject(message),
                    TargetArn = _snsWorkflowApproveTaskArn
                };
                try
                {
                    var response = await client.PublishAsync(request);
                    return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    LambdaLogger.Log("Error with sending SNS message .  Details: " + ex.Message);
                    return false;
                }
            }
        }
    }
}
