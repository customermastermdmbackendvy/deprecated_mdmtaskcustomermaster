﻿
using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Context;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
        }

        public async Task<MdmUserDomain> Retrieve(string userId)
        {
            using (var mdmContext = GetContext())
            {
                var mdmUser = await mdmContext.MdmUser.Where(x => x.Value.ToLower() == userId.ToLower()).FirstOrDefaultAsync();
                return Mapper.Map<MdmUserDomain>(mdmUser);
            }
        }

     
    }
}
