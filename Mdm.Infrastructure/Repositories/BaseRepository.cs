﻿
using AutoMapper;
using Mdm.Context;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        private MdmContext _mdmContext;
        private IDbContextTransaction _transaction;
        private string _connectionString;

        protected MdmContext MdmContext
        {
            get
            {
                if (_mdmContext == null)
                    _mdmContext = new MdmContext(_connectionString);
                return _mdmContext;
            }
        }

        protected MdmContext GetContext()
        {
            return new MdmContext(_connectionString);
        }

        protected IMapper Mapper { get; private set; }

        protected async Task<bool> StartTransaction()
        {
            if (_transaction == null)
                _transaction = await MdmContext.Database.BeginTransactionAsync();

            return true;
        }

        public BaseRepository(IConnectionManager connectionManager, IMapper mapper)
        {
            _connectionString = connectionManager.ConnectionString;
            Mapper = mapper;
        }

        public void Dispose()
        {
            if (_mdmContext != null)
            {
                _mdmContext.Dispose();
                _mdmContext = null;
            }

            if (_transaction != null)
            {
                _transaction = null;
            }
        }

        public void Commit()
        {
            if (_transaction != null)
            {
                _transaction.Commit();
            }
            Dispose();
        }

        public void Rollback()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
            }
            Dispose();
        }
    }
}
