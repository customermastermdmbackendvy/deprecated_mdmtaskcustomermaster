﻿
using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Context;
using Mdm.Context.Entities;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Interfaces.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Mdm.Infrastructure.Repositories
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public CustomerRepository(IConnectionManager connectionManager, IMapper mapper) : base(connectionManager, mapper)
        {
        }

        public WorkflowApolloCustomerMasterDomain Retrieve(string workflowId)
        {

            using (var mdmContext = GetContext())
            {
                var workflowApolloCustomerMaster = mdmContext.WorkflowApolloCustomerMaster.Where(x => x.WorkflowId == workflowId).FirstOrDefault();
                return Mapper.Map<WorkflowApolloCustomerMasterDomain>(workflowApolloCustomerMaster);
            }

        }

        public async Task<bool> Update(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain)
        {

            MdmContext.Update(Mapper.Map<WorkflowApolloCustomerMaster>(workflowApolloCustomerMasterDomain));
            if (MdmContext.ChangeTracker.HasChanges())
                return await MdmContext.SaveChangesAsync() > 0;
            else
            {
                LambdaLogger.Log("Requested updates for Apollo Global Customer Master record of workflow Id " + workflowApolloCustomerMasterDomain.WorkflowId + " are the same as the current record.");
                return false;
            }
        }


        public async Task<bool> Create(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain)
        {
            MdmContext.Add(Mapper.Map<WorkflowApolloCustomerMaster>(workflowApolloCustomerMasterDomain));
            return await MdmContext.SaveChangesAsync() > 0;

        }

        public WorkflowCustomerGlobalDomain RetrieveCustomerGlobal(string workflowId)
        {
            using (var mdmContext = GetContext())
            {
                var workflowCustomerGlobalMaster = mdmContext.WorkflowCustomerGlobal.Where(x => x.WorkflowId == workflowId).FirstOrDefault();
                return Mapper.Map<WorkflowCustomerGlobalDomain>(workflowCustomerGlobalMaster);
            }
        }

        public async Task<bool> UpdateCustomerGlobal(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain)
        {
            MdmContext.Update(Mapper.Map<WorkflowCustomerGlobal>(workflowCustomerGlobalDomain));
            if (MdmContext.ChangeTracker.HasChanges())
                return await MdmContext.SaveChangesAsync() > 0;
            else
            {
                LambdaLogger.Log("Requested updates for apollo customer global record of workflow Id " + workflowCustomerGlobalDomain.WorkflowId + " are the same as the current record.");
                return false;
            }
        }

        public async Task<bool> CreateCustomerGlobal(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain)
        {
            MdmContext.Add(Mapper.Map<WorkflowCustomerGlobal>(workflowCustomerGlobalDomain));
            return await MdmContext.SaveChangesAsync() > 0;
        }
    }
}
