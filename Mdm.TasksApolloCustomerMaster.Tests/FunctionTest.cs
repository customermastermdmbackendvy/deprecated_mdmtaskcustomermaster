using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using Microsoft.Extensions.Configuration;
using System.IO;
using Mdm.Services.Models;
using Mdm.TasksApolloCustomerMaster;
using Mdm.Services.Helpers;
using Newtonsoft.Json;

namespace Mdm.TasksApolloCustomerMaster
{
    public class FunctionTest
    {
        private const string EnvironmentRegion = "EnvironmentRegion";
        private const string RdsSecretName = "RdsSecretName";
        private const string SimpleNotificationServiceArn = "SnsUpdateWorkflowArn";
        private const string Environment = "Environment";
        private const string WorkflowUpdateUrl = "WorkflowUpdateUrl";

        public FunctionTest()
        {
            SetEnvironmentVariables();

        }
        private void SetEnvironmentVariables()
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();


            System.Environment.SetEnvironmentVariable(EnvironmentRegion, configuration[EnvironmentRegion]);
            System.Environment.SetEnvironmentVariable(RdsSecretName, configuration[RdsSecretName]);
            System.Environment.SetEnvironmentVariable(SimpleNotificationServiceArn, configuration[SimpleNotificationServiceArn]);
            System.Environment.SetEnvironmentVariable(Environment, configuration[Environment]);
            System.Environment.SetEnvironmentVariable(WorkflowUpdateUrl, configuration[WorkflowUpdateUrl]);
        }

        //[Fact]
        //public async void TestSuccessRejectUpdateFunction()
        //{
        //    var function = new MdmTasksApolloGlobalTrade();
        //    var context = new TestLambdaContext();

        //    var workflowApolloGlobalTradeModel1 = new WorkflowApolloGlobalTradeModel
        //    {
        //        AdditionalNotes = "Additional notes - " + System.Guid.NewGuid().ToString(),

        //        WorkflowTaskModel = new WorkflowTaskModel
        //        {
        //            RejectReason = "Bad",
        //            TaskId = 123,
        //            UserId = "globaltrade.user",
        //            WorkflowId = "wf12345678",
        //            WorkflowTaskStateChangeType = Services.Helpers.WorkflowOperation.Reject
        //        }
        //    };

        //    var serializedSample = JsonConvert.SerializeObject(workflowApolloGlobalTradeModel1);
        //    var operationResultReject1 = await function.SaveTaskGlobalTrade(workflowApolloGlobalTradeModel1, context);


        //    Assert.True(operationResultReject1.IsSuccess);
        //}

        //[Fact]
        //public async void TestSuccessRejectAddFunction()
        //{
        //    var function = new MdmTasksApolloGlobalTrade();
        //    var context = new TestLambdaContext();

        //    var workflowApolloGlobalTradeModel1 = new WorkflowApolloGlobalTradeModel
        //    {
        //        AdditionalNotes = "Additional notes - " + System.Guid.NewGuid().ToString(),
        //        WorkflowTaskModel = new WorkflowTaskModel
        //        {
        //            RejectReason = "Bad",
        //            TaskId = 123,
        //            UserId = "globaltrade.user",
        //            WorkflowId = Utilities.GetUniqueID("wf", 15),
        //            WorkflowTaskStateChangeType = Services.Helpers.WorkflowOperation.Reject
        //        }
        //    };


        //    var operationResultReject1 = await function.SaveTaskGlobalTrade(workflowApolloGlobalTradeModel1, context);


        //    Assert.True(operationResultReject1.IsSuccess);
        //}

        //[Fact]
        //public async void TestSuccessApproveUpdateFunction()
        //{
        //    var function = new MdmTasksApolloGlobalTrade();
        //    var context = new TestLambdaContext();

        //    var workflowApolloGlobalTradeModel1 = new WorkflowApolloGlobalTradeModel
        //    {
        //        AdditionalNotes = "Additional notes - " + System.Guid.NewGuid().ToString(),
        //        WorkflowTaskModel = new WorkflowTaskModel
        //        {
        //            TaskId = 123,
        //            UserId = "globaltrade.user",
        //            WorkflowId = "wf12345678",
        //            WorkflowTaskStateChangeType = Services.Helpers.WorkflowOperation.Approve
        //        }
        //    };


        //    var operationResultReject1 = await function.SaveTaskGlobalTrade(workflowApolloGlobalTradeModel1, context);


        //    Assert.True(operationResultReject1.IsSuccess);
        //}

   
    
    }
}
