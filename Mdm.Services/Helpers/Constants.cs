﻿

namespace Mdm.Services.Helpers
{
    public enum WorkflowEngineRequestType
    {
        Workflow = 1,
        Task = 2
    }
    public enum OperationalResultType
    {
        Success = 1,
        Validation = 2,
        Error = 3
    }

    public enum WorkflowType
    {
        CreateSoldToSapApollo = 1,
        CreateShipToSapApollo = 2,
        CreateBillToSapApollo = 3,
        CreatePayerSapApollo = 4,
        CreateSalesRepSapApollo = 5,
        CreateDropShipSapApollo = 6,
        CreateSoldToSapOlympus = 7,
        CreateShipToSapOlympus = 8,
        CreateBillToSapOlympus = 9,
        CreatePayerSapOlympus = 10,
        CreateSalesRepSapOlympus = 11,
        CreateSoldToPointman = 12,
        CreateShipToPointman = 13,
        CreateSalesRepPointman = 14,
        CreateSoldToMade2Manage = 15,
        CreateShipToMade2Manage = 16,
        CreateSalesRepMade2Manage = 17,
        CreateSoldToJDEdwards = 18,
        CreateShipToJDEdwards = 19,
        CreateSalesRepJDEdwards = 20,
        UpdateSapApollo = 21,
        ExtendSapApollo = 22,
        BlockSapApollo = 23
    }

    public enum WorkflowStateType
    {
        New=1,
        InProgress=2,
        Completed=3,
        Withdrawn=4
    }
    public enum WorkflowTaskOperationType
    {
        None = 0,
        ApproveTask = 1,
        RejectTask = 2,
        GetUserTasks = 3
    }

    public enum WorkflowTaskStateType
    {
        New        = 1,
	    InProgress = 2,
	    Approved   = 3,
	    Rejected   = 4
    }
    public enum WorkflowTaskType
    {
        ApproveSoldTo = 1,
        UnblockSoldTo = 2,
        ApproveShipTo = 3,
        ApproveBillTo = 4,
        ApprovePayer  = 5
    }

    public enum WorkflowTeamType
    {
        CustomerServiceSales = 1,
        GlobalTrade = 2,
        CustomerMaster = 3,
        Credit = 4,
        Contracts = 5,
        SalesOperations = 6,
        CustomerServiceOperations = 7,
        Pricing = 8,
        Tax =9
    }
}

