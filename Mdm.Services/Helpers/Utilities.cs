﻿using System;

namespace Mdm.Services.Helpers
{
    public class Utilities
    {
        public static string GetUniqueID(string prefix, int length)
        {
            return prefix + string.Format("{0:d"+ length + "}", (DateTime.UtcNow.Ticks / 10) % 1000000000);
        }
    }
}
