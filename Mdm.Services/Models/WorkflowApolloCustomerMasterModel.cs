﻿
using System;

namespace Mdm.Services.Models
{
    public partial class WorkflowApolloCustomerModel
    {
        public WorkflowTaskRequest WorkflowTaskModel { get; set; }
 
        public string License { get; set; }
        public DateTime? LicenseExpDate { get; set; }
        public string SearchTerm1 { get; set; }
        public string SearchTerm2 { get; set; }
        public int CustomerClassTypeId { get; set; }
        public int IndustryCodeTypeId { get; set; }
        public string TransporationZone { get; set; }
        public int IndustryTypeId { get; set; }
        public string TaxNumber2 { get; set; }
        public int ReconAccountTypeId { get; set; }
        public string SortKey { get; set; }
        public string PaymentHistoryRecord { get; set; }
        public string PaymentMethods { get; set; }
        public string AcctgClerk { get; set; }
        public string AccountStatement { get; set; }
        public int SalesOfficeTypeId { get; set; }
        public int PpcustProcTypeId { get; set; }
        public int DeliveryPriorityTypeId { get; set; }
        public int ShippingConditionsTypeId { get; set; }
        public bool OrderCombination { get; set; }
        public int Incoterms1TypeId { get; set; }
        public string Incoterms2 { get; set; }
        public int AcctAssignmentGroupTypeId { get; set; }
        public string TaxClassification { get; set; }
        public int? PartnerFunctionTypeId { get; set; }
        public string PartnerFunctionNumber { get; set; }
        public int ShippingCustomerTypeId { get; set; }
        public string AdditionalNotes { get; set; }


       // public string MdmCustomerId { get; set; }
        //public string SystemRecordId { get; set; }
      //  public string Title { get; set; }
      //  public string Name1 { get; set; }
      //  public string Name2 { get; set; }
      //  public string Name3 { get; set; }
      //  public string Name4 { get; set; }
      //  public string Street { get; set; }
      //  public string Street2 { get; set; }
      //  public string City { get; set; }
      //  public string Region { get; set; }
      //  public string PostalCode { get; set; }
      //  public string Country { get; set; }
      //  public string VatRegNo { get; set; }
      //  public string Telephone { get; set; }
      //  public string Fax { get; set; }
      //  public string Email { get; set; }
      //  public string TaxNumber { get; set; }
      //  public string DunsNumber { get; set; }
      //  public string NaicsCode { get; set; }
      //  public string NaicsDescription { get; set; }
      //  public string SicCode4 { get; set; }
      //  public string SicCode6 { get; set; }
      //  public string SicCode8 { get; set; }
      //  public string Purpose { get; set; }
      //  public int CategoryTypeId { get; set; }
      //  //public int? RoleTypeId { get; set; }
      // // public int? SalesOrgTypeId { get; set; }
      ////  public int SystemTypeId { get; set; }
      //  public int CompanyCodeTypeId { get; set; }
      //  public int DistributionChannelTypeId { get; set; }
      //  public int DivisionTypeId { get; set; }
      //  public DateTime EffectiveDate { get; set; }


        public int CreatedUserId { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
