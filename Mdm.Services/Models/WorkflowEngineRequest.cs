﻿using Mdm.Services.Helpers;
using Mdm.Services.Models;
using Newtonsoft.Json;

namespace Mdm.WorkflowEngineServices.Models
{
    public class WorkflowEngineRequest
    {
        [JsonProperty(PropertyName = "workflowenginerequesttype", NullValueHandling = NullValueHandling.Ignore)]
        public WorkflowEngineRequestType WorkflowEngineRequestType { get; set; }

        [JsonProperty(PropertyName = "workflowtaskrequest", NullValueHandling = NullValueHandling.Ignore)]
        public WorkflowTaskRequest WorkflowTaskRequest { get; set; }
    }
}
