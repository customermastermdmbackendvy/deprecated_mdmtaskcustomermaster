﻿using Mdm.Services.Helpers;


namespace Mdm.Services.Models
{
    public class WorkflowTaskRequest
    {
        public WorkflowTaskRequest()
        {
            RejectReason = string.Empty;
            UserId = string.Empty;
            WorkflowId = string.Empty;
        }
        public string RejectReason { get; set; }
        public WorkflowTaskOperationType WorkflowTaskOperationType { get; set; }
        public string UserId { get; set; }
        public string WorkflowId { get; set; }
        public int TaskId { get; set; }
    }
}
