﻿
using Mdm.Services.Helpers;
using System.Collections.Generic;


namespace Mdm.Services.Models
{
    public class OperationResult<T> where T : class
    {
        public OperationResult()
        {
            OperationResultMessages = new List<OperationResultMessage>();
        }
        public string OperationName { get; set; }

        public bool IsSuccess { get; set; }

        public List<OperationResultMessage> OperationResultMessages { get; }
        public T ResultData { get; set; }
    }
}
