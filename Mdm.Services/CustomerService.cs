﻿using System.Threading.Tasks;
using AutoMapper;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Domain.Interfaces.Managers;
using Mdm.Domain.Entities;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;
using Mdm.Services.Helpers;
using System;
using Mdm.WorkflowEngineServices.Models;

namespace Mdm.Services
{
    public class CustomerService : BaseCustomerService, ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISnsManager _snsManager;
        public CustomerService(IBaseRepository iBaseRepository, ICustomerRepository customerRepository, IUserRepository userRepository, ISnsManager snsManager,  IMapper mapper) : base(mapper, iBaseRepository)
        {
            _customerRepository = customerRepository;
            _userRepository = userRepository;
            _snsManager = snsManager;
        }

        public async Task<OperationResult<WorkflowApolloCustomerModel>> ProcessApolloTaskCustomerMaster(WorkflowApolloCustomerModel workflowApolloCustomerMasterModel)
        {
            InitOperationResult(workflowApolloCustomerMasterModel);
            ValidateRequest(workflowApolloCustomerMasterModel);
            if (!OperationResult.IsSuccess)
            {
                return OperationResult;
            }

            var workFlowTaskRequest = workflowApolloCustomerMasterModel.WorkflowTaskModel;
            var newApolloCustomerMasterData = Mapper.Map<WorkflowApolloCustomerMasterDomain>(workflowApolloCustomerMasterModel);
     //comment for WfGlobal
            var newWorkflowCustomerGlobalDomainData = Mapper.Map<WorkflowCustomerGlobalDomain>(workflowApolloCustomerMasterModel);

            try
            {
                // Only Save for Approve
                if (workFlowTaskRequest.WorkflowTaskOperationType == WorkflowTaskOperationType.ApproveTask)
                {
                    PopulateUserIds(newApolloCustomerMasterData);
                    if (!OperationResult.IsSuccess)
                    {
                        return OperationResult;
                    }

                    var existingApolloCustomerMasterData = _customerRepository.Retrieve(workFlowTaskRequest.WorkflowId);
         //comment for WfGlobal
                   // var existingApolloCustomerGlobalData = _customerRepository.RetrieveCustomerGlobal(workFlowTaskRequest.WorkflowId);
                    if (existingApolloCustomerMasterData == null)
                    {
                        if (!await ProcessApolloCustomerMasterCreate(newApolloCustomerMasterData, workFlowTaskRequest.TaskId))
                            return OperationResult;
                    }
                    else
                    {
                        if (!await ProcessApolloCustomerMasterUpdate(MergeDomains(newApolloCustomerMasterData, existingApolloCustomerMasterData), workFlowTaskRequest.TaskId))
                            return OperationResult;
                    }

                    //comment for WfGlobal
                    //if (existingApolloCustomerGlobalData == null)
                    //{
                    //    if (!await ProcessApolloCustomerGlobalCreate(newWorkflowCustomerGlobalDomainData, workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //    if (!await ProcessApolloCustomerMasterCreate(newApolloCustomerMasterData, workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //}
                    //else if (existingApolloCustomerGlobalData != null && existingApolloCustomerMasterData == null)
                    //{
                    //    if (!await ProcessApolloCustomerMasterCreate(newApolloCustomerMasterData, workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //    if (!await ProcessApolloCustomerGlobalUpdate(MergeDomains(newWorkflowCustomerGlobalDomainData, existingApolloCustomerGlobalData), workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //}
                    //else if (existingApolloCustomerGlobalData != null && existingApolloCustomerMasterData != null)
                    //{
                    //    if (!await ProcessApolloCustomerMasterUpdate(MergeDomains(newApolloCustomerMasterData, existingApolloCustomerMasterData), workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //    if (!await ProcessApolloCustomerGlobalUpdate(MergeDomains(newWorkflowCustomerGlobalDomainData, existingApolloCustomerGlobalData), workFlowTaskRequest.TaskId))
                    //        return OperationResult;
                    //}



                    IBaseRepository.Commit();
                }

                // Publish Approve message to Topic that handles Worlflow Updates.

                if (!await UpdateWorkflow(workFlowTaskRequest))
                {
                    return OperationResult;
                }

                OperationResult.OperationResultMessages.Add
                (
                    new OperationResultMessage { Message = "Successful update of task id " + workFlowTaskRequest.TaskId, OperationalResultType = OperationalResultType.Success }
                );
                OperationResult.IsSuccess = true;
            }
            catch(Exception ex)
            {
                ProcessOperationError("Unexpected error: " + ex.Message);
            }
            return OperationResult;
        }
       
        #region Private Helpers
        private void ValidateRequest(WorkflowApolloCustomerModel workflowApolloCustomerMasterModel)
        {
            if(workflowApolloCustomerMasterModel ==null || 
                workflowApolloCustomerMasterModel.WorkflowTaskModel == null ||
                string.IsNullOrEmpty(workflowApolloCustomerMasterModel.WorkflowTaskModel.WorkflowId) ||
                 (workflowApolloCustomerMasterModel.WorkflowTaskModel.WorkflowTaskOperationType != WorkflowTaskOperationType.ApproveTask && workflowApolloCustomerMasterModel.WorkflowTaskModel.WorkflowTaskOperationType != WorkflowTaskOperationType.RejectTask) ||
                workflowApolloCustomerMasterModel.WorkflowTaskModel.WorkflowTaskOperationType == WorkflowTaskOperationType.RejectTask && string.IsNullOrEmpty(workflowApolloCustomerMasterModel.WorkflowTaskModel.RejectReason)
                )
                
            {
                var msg = "Invalid WorkflowApolloGlobalCustomerMasterModel request";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
            }
            else
            {
                OperationResult.IsSuccess = true;
            }
           
        }

        private void PopulateUserIds(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain)
        {
            var user = _userRepository.Retrieve(workflowApolloCustomerMasterDomain.UserId);
            if (user == null)
            {
                var msg = "User " + workflowApolloCustomerMasterDomain.UserId + " is not found";
                ProcessOperationError(msg);
                OperationResult.IsSuccess = false;
            }
            workflowApolloCustomerMasterDomain.ModifiedUserId = user.Id;
            workflowApolloCustomerMasterDomain.CreatedUserId = user.Id;
            OperationResult.IsSuccess = true;
        }

        private async Task<bool> UpdateWorkflow(WorkflowTaskRequest workflowTaskRequest)
        {
            var workflowEngineRequest = new WorkflowEngineRequest
            {
                WorkflowEngineRequestType = WorkflowEngineRequestType.Task,
                WorkflowTaskRequest = workflowTaskRequest
            };

            return await _snsManager.SendSnsMessage(workflowEngineRequest);
        }

        private async Task<bool> ProcessApolloCustomerMasterCreate(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain, int taskId)
        {
            // Create if no data exists yet
            OperationResult.IsSuccess = await _customerRepository.Create(workflowApolloCustomerMasterDomain);
            if (!OperationResult.IsSuccess)
            {
                var msg = "There was an error creating new Apollo Customer Master record for task id " + taskId;
                ProcessOperationError(msg);
                return false;
            }
            return true;
        }

        private async Task<bool> ProcessApolloCustomerMasterUpdate(WorkflowApolloCustomerMasterDomain workflowApolloCustomerMasterDomain, int taskId)
        {
            // Create if no data exists yet
            OperationResult.IsSuccess = await _customerRepository.Update(workflowApolloCustomerMasterDomain);
            if (!OperationResult.IsSuccess)
            {
                var msg = "There was an error updated Apollo Customer Master record for task id " + taskId;
                ProcessOperationError(msg);
                return false;
            }
            return true;
        }

        private async Task<bool> ProcessApolloCustomerGlobalCreate(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain, int taskId)
        {
            // Create if no data exists yet
            OperationResult.IsSuccess = await _customerRepository.CreateCustomerGlobal(workflowCustomerGlobalDomain);
            if (!OperationResult.IsSuccess)
            {
                var msg = "There was an error creating new Apollo Customer Master record for task id " + taskId;
                ProcessOperationError(msg);
                return false;
            }
            return true;
        }

        private async Task<bool> ProcessApolloCustomerGlobalUpdate(WorkflowCustomerGlobalDomain workflowCustomerGlobalDomain, int taskId)
        {
            // Create if no data exists yet
            OperationResult.IsSuccess = await _customerRepository.UpdateCustomerGlobal(workflowCustomerGlobalDomain);
            if (!OperationResult.IsSuccess)
            {
                var msg = "There was an error updated Apollo Customer Master record for task id " + taskId;
                ProcessOperationError(msg);
                return false;
            }
            return true;
        }
        #endregion
    }
}
