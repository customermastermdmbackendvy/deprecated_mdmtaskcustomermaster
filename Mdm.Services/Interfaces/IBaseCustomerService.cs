﻿using AutoMapper;

using Mdm.Services.Models;
using System.Collections.Generic;

namespace Mdm.Services.Interfaces
{
    public interface IBaseCustomerService
    {
        IMapper Mapper { get; }
        void ProcessOperationError(string message, string operationName);
        OperationResult<WorkflowApolloCustomerModel> OperationResult { get; set; }
    }
}
