﻿using Mdm.Services.Models;
using System.Threading.Tasks;

namespace Mdm.Services.Interfaces
{
    public interface ICustomerService : IBaseCustomerService
    {
        Task<OperationResult<WorkflowApolloCustomerModel>> ProcessApolloTaskCustomerMaster(WorkflowApolloCustomerModel workflowApolloPricingModel);
    }
}
