﻿

using Amazon.Lambda.Core;
using AutoMapper;
using Mdm.Domain.Entities;
using Mdm.Domain.Interfaces.Repositories;
using Mdm.Services.Helpers;
using Mdm.Services.Interfaces;
using Mdm.Services.Models;

namespace Mdm.Services
{
    public class BaseCustomerService : IBaseCustomerService
    {
        protected IBaseRepository IBaseRepository;
        public IMapper Mapper { get; private set; }

        public OperationResult<WorkflowApolloCustomerModel> OperationResult { get; set; }
        public BaseCustomerService(IMapper mapper, IBaseRepository baseRepository) 
        {
            Mapper = mapper;
            IBaseRepository = baseRepository;
        }

        public void ProcessOperationError(string message, string operationName)
        {
            OperationResult = new OperationResult<WorkflowApolloCustomerModel>
            {
                OperationName = operationName,
                ResultData = new WorkflowApolloCustomerModel()
            };

            ProcessOperationError(message);
        }
        protected void ProcessOperationError(string message)
        {
            OperationResult.OperationResultMessages.Add
            (
                new OperationResultMessage { Message = message, OperationalResultType = OperationalResultType.Error }
            );
            OperationResult.IsSuccess = false;
            LambdaLogger.Log(message);
            IBaseRepository.Rollback();
        }

        protected void InitOperationResult(WorkflowApolloCustomerModel workflowApolloCustomerMasterModel)
        {
            OperationResult = new OperationResult<WorkflowApolloCustomerModel>
            {
                OperationName = "ProcessApolloTaskCustomerMasterData",
                ResultData = workflowApolloCustomerMasterModel
            };
        }

        protected WorkflowApolloCustomerMasterDomain MergeDomains(WorkflowApolloCustomerMasterDomain source, WorkflowApolloCustomerMasterDomain dest)
        {
            source.Id = dest.Id;
            source.CreatedUserId = dest.CreatedUserId;
            source.CreatedOn = dest.CreatedOn;
            return Mapper.Map(source, dest);
        }
        protected WorkflowCustomerGlobalDomain MergeDomains(WorkflowCustomerGlobalDomain source, WorkflowCustomerGlobalDomain dest)
        {
            source.Id = dest.Id;
            source.CreatedUserId = dest.CreatedUserId;
            source.CreatedOn = dest.CreatedOn;
            return Mapper.Map(source, dest);
        }
    }
}
